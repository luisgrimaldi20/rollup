# Rollup



## Getting started

The ZK-Rollup schematic consists of two types of users: transactors and repeaters. Transactos create their transfer and transmit it to the web. Transfer data consists of an indexed address of "receiver" and "sender", a cost to perform the transaction, the network cost, and the nonce. An abbreviated indexed version of 3 bytes of the addresses reduces the resource processing requirements. The cost of a transaction that is higher or lower than zero creates a deposit or withdrawal, respectively.

ZK-Rollups are one of the options that are being developed for the construction of Layer 2 that augments the scalability through the processing of massive transfers in a single transaction. ZK-Rollups group hundreds of transfers in a single transaction. The smart contract will deconstruct and verify each transfer made in a single transaction.

Ommyo Chain is based on the ZK-Rollup. ZK-Rollup is an L2 scaling solution in which all funds are maintained through a smart contract in the main chain, while the computing and storage is done off chain.



## Payments

There are two kinds of Ommyo Chain operations:

Priority Operations: 

Currently there exist the following two kinds of priority operations:

```Deposit```: Moves funds from the Ethereum network to the designated account in the Ommyo Chain network. If the receiving account does not yet exist in the Ommyo Chain, it will be created and assigned a numeric identification to the address provided.

```FullExit```: Withdraws funds from the Ommyo Chain network to the Ethereum network without interacting with the Ommyo Chain server. 
This operation can be used as an emergency exit in case of censure detection from the Ommyo Chain server node, or to withdraw funds in a situation such as not being able to configure the signing key for an account in the Ommyo Chain network (e.g. if the address belongs to an smart contract).

## Transactions

Transactions must be sent through the Ommyo Chain operator using the API. They are identified by their serialized representation hash. Currently, the following kinds of transactions exist:

```ChangePubKey```: Establishes (or changes) the signing key of an associated account. Without a group of signing keys, no operation (except priority operations) can be authorized by the corresponding account.

```Transfer```: Transfers funds from one Ommyo Chain account to another Ommyo Chain account. If the receiving account does not yet exist in the Ommyo Chain, it will be created and assigned a numeric identification to the address provided.

```Swap```: Interchanges funds automatically between two existing Ommyo Chain accounts.

```Withdraw```: Withdraws funds from the Ommyo Chain network to the Ethereum network.

```ForcedExit```: Withdraws funds from the L2 "objective" account that doesn't have an established signing key to the same "objective" address in the Ethereum network. This operation can be used to withdraw funds in case of not being able to configure the signing key of the Ommyo Chain network account (e.g. if the address belongs to an smart contract.

```MintNFT```: Mints an NFT based on the provided content hash to the "receiver" provided.

```WithdrawNFT```: Withdraws an NFT from the Ommyo Chain network to the Ethereum network.


## Blocks

All operations within Ommyo Chain are organized in blocks. After the Ommyo Chain operator creates a block, the Ommyo Chain smart contract is sent to the main Ethereum network with a ```Commit``` transaction. When a block is confirmed, its state is not yet definite. After a few minutes, the ZK proof of the block's exactitud is produced. This proof is published on Ethereum using the ```Verify``` transaction. Only after ```Verify``` has extracted the tx is the new state considered definite. Several blocks can be confirmed but they have yet to be verified.

However, the execution model is slightly different: in order to not make users wait until the finalization of the block, transactions are grouped into "mini-blocks" with a much shorter wait time. This way, blocks are partially applied with a short time interval, so that shortly after receiving the transaction it is executed and the L2 state is updated correspondingly.

## Flow

This section describes typical use cases for Ommyo Chain in a sequential manner.

#### Account Creation

Ommyo Chain accounts can be created by making a deposit of funds from Ethereum or transferring funds in Ommyo Chain to the address desired. Either of these actions will create a new account in the Ommyo Chain network if it doesn't exist. However, recently created accounts cannot authorize any transactions. In order to do so, the owner of the account must configure the signing key for their account.

#### Signing key Configuration

The default value of every signing key of every account is established as zero, which sets the account as "no owner". This is a requirement for the following reasons:
- If a transfer to an address is valid on Ethereum, then it is also valid on Ommyo Chain.
- Not all addresses can have a private password (e.g. some smart contracts).
- Transfers to the account from a user can occur before they become interested in Ommyo Chain.

As such, in order for an account to be able to perform L2 transfers the user must establish a signing key through the ChangePubKey transaction.

This transaction has two signatures:
- Ommyo Chain signature of the data in the transaction, making it impossible to change the contents of the transaction;
- Ethereum signature that proves the ownership of the account.

The Ethereum signature must be a signature of a predetermined message. It is also possible for the ChangePubKey to chain authorize the operation by sending a corresponding transaction to the Ommyo Chain smart contract. Both the Ommyo Chain server as well as the smart contract verify the ownership of the account in order to obtain better security guarantees.

## Tranfers of Funds
As previously mentioned, any transfer that is valid on Ethereum is valid on Ommyo Chain. Users can transfer any amount of funds on Ether or any ERC-20 compatible token. A list of approved tokens can be found in the corresponding browser.

However, the transfer to an non-existent account requires more data to be sent with the smart contract (including information about the new account) which means the fee for such transfers is slightly higher than the fee for transfers to existing accounts.

## Fees
Ommyo Chain requires fees for transactions in order to cover network maintenance costs. The fees for each type of transaction are calculated based on three main factors:

- Amount of data sent to the Ethereum network
- Current price of gas
- Cost of computational resources in order to generate proof for a block with the transaction.

Given that we include many transactions in a block, the cost can be spread across all the included transactions, resulting in very low fees. In addition, the API provides all the entry data used for the fee calculation through the corresponding API method.

## Withdrawing funds

Currently, there are three ways to withdraw funds from Ommyo Chain to an Ethereum account.
The first way is with a ```Withdraw``` transaction. 
This is an L2 transaction that can be used to solicit a withdrawal from an account to any Ethereum address. 
Like any other transfer, it must be signed by the correct private Ommyo Chain key.

This is the preferred method for situations in which one is the owner of an account and has the signing key for it.

The second way is ```ForcedExit```. This is an L2 transaction that can be used to solicit a withdrawal from any account without an owner (one that doesn't have a group of signing keys). 
In this transaction one cannot choose the address or the amount of Ethereum: the only option is to solicit the withdrawal of all the disposable funds of a determined token to the L2 address to the L1 address.

In addition, the initiator of the transaction must cover the fee which is exactly the same as with the Withdraw operation.
Además, el iniciador de la transacción debe cubrir la tarifa, que es exactamente la misma que para la Withdraw operación.

This is the preferred method if the funds must be withdrawn from an account that cannot configure a signing key (i.e. an smart contract account) and there is an L2 account that can send this sort of transaction.

The third way is a ```FullExit```, this is a priority operation. This type of operation is called a "priority operation" due to it being initiated from the L1, and the smart contract offers guarantees that the request will be processed within a reasonable time interval or the network will be deemed compromised/dead and the contract will enter an exodus mode.

This is the preferred method should the use experience the censure of the network operator.

## Smart contracts
#### Programming Model
The programming model for Ommyo Chain's smart contracts is inherited from Ethereum. To begin with, it permits Solidity, which means it can use unlimited loops, recursivity, vectors, and arbitrary longitud maps, etc. 

Local variables are stored in the pile or the mound, while the contract's storage is accessed globally. Contracts can contact each other through heavily encrypted interfaces and have access to public storage camps.

#### Composability
Ommyo Chain smart contracts can contact each other in the same way Ethereum smart contracts can. Each transaction call tree is atomic, independent of the amount of instances of contract involved.

Any DeFi project can be migrated to Ommyo Chain, as most of the Solidity code can be implemented without any changes.

#### zkEVM
The zkEVM is a highly efficient virtual machine, Turing tested and compatible with SNARK in order to execute smart contracts. It applies latest generation optimizations to the code of the smart contract, while the virtual machine itself is optimized for elevated charges, which allows it to perform transactions in the blink of an eye.
The machine is compatible with SNARK; that is to say, the execution path is traceable in SNARK. However, it does not require a circuit per program. In its place, a single circuit which must be audited only once may be used. The test system for Sync VM is PLONK.

#### Solidity

##### Smart contract portability

Most DeFi and NFT projects will function without any changes in code. However, in the first version, the compiler will automatically replace the SHA256 and Keccak256 calls with a hash function compatible with circuits. Some other primitive cryptographics are likewise incompatible, such as ecrecover and cryptographic precompilations.

#### User Interface Interaction

It can fully interact with smart contracted and the Ommyo Chain network through our API Web3 and Ether SDK:
For read solicitations: any compatible framework with web3 in any language will immediately work, with the additional and optional functionality specific to Ommyo Chain L2.

For write solicitations (send transactions): due to the fundamental differences between L1 and L2, additional code will need to be written (e.g. Ommyo Chain allows fee payment in any token, which is why the transaction will imply choosing a token to pay the fees).

#### Transaction
For interactions with smart contracts, users will sign an EIP712 message with a hash of the call data. Given that EIP712 is based on a native Ethereum signature, all wallets, including hardware wallets, will function without the need for extensions.

### Deployment

#### Zinc

Zinc is an emerging framework to develop smart contracts and SNARK circuits on the Ommyo Chain platform. Existing ZKP frameworks lack the specific functionality for smart contracts. Given that smart contracts handle valuable financial assets, security and protection are crucial.

Zinc is created to fill the gap between these two worlds, providing a language for smart contracts that is simple and dependable, optimized for ZKP circuits, and easy for developers to learn.

The framework includes a simple language, complete Turing, centered around security and with general purpose, designed specifically to develop smart contracts and zero-knowledge test circuits with a flat learning curve. 
The syntax and semantics are very similar to those of Rust. The Zinc compiler uses LLVM as its medium and back-end, which provides a powerful group of solutions to optimize the code.



***

## License
For open source projects, say how it is licensed.


