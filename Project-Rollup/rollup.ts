// app.js
const WebSocket = require('ws');
const zksync = require('zksync');

async function main() {
  // Get the provider. It's important to specify the correct network.
  const provider = await zksync.getDefaultProvider('mainnet');
  // Connect to the event server.
  const ws = new WebSocket('wss://events.zksync.io/');
  console.log('Connection established');
  // Change the address to the account you're intrested in.
  const ACCOUNT_ADDRESS = '0x7ca2113e931ada26f64da66822ece493f20059b6';

  // Once connected, start sending ping frames.
  setInterval(function () {
    ws.ping();
  }, 10000);

  // Register filters.
  ws.on('open', function open() {
    ws.send('{}');
  });

  ws.on('close', function close(code, reason) {
    console.log(`Connection closed with code ${code}, reason: ${reason}`);
  });

  ws.on('message', function (data) {
    const event = JSON.parse(data);

    // We are looking for transfers to the specific account.
    if (event.type == 'transaction' && event.data.tx.type == 'Transfer') {
      const recipient = event.data.tx.to;
      if (recipient != ACCOUNT_ADDRESS) {
        return;
      }
      // Use the provider for formatting.
      const token = provider.tokenSet.resolveTokenSymbol(event.data.token_id);
      const amount = provider.tokenSet.formatToken(token, event.data.tx.amount);

      const status = event.data.status;
      const fromAddr = event.data.tx.from;
      const blockNumber = event.block_number;

      console.log(`There was a transfer to ${recipient}`);
      console.log(`Block number: ${blockNumber}`);
      console.log(`From: ${fromAddr}`);
      console.log(`Token: ${token}`);
      console.log(`Amount: ${amount}`);
      console.log(`Status: ${status}\n`);
    }
  });
}

main();